package stream.series.util;

import java.io.Serializable;
import java.util.Vector;

/**
 * This is a simple sliding window class with constant length window. It accepts every {@code Serializable} value.
 * @author Markus Kokott ( markus.kokott(at)udo.edu )
 * 14.02.2012
 */
public class SlidingWindow extends Window{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5688068099579008211L;

	public SlidingWindow(Integer windowSize) {
		super(windowSize);
	}

	@Override
	public Vector<Serializable> getWindow(String key) {
		
		return super.getWindow().get(key);
	}

	@Override
	public void updateWindow(String key, Serializable data) {
		
		// create new window, if this key hasn't appeared yet.
		if (!this.containsKey(key)){
			
			super.getKeys().add(key);
			super.getWindow().put(key, new Vector<Serializable>());
		}
		// insert new element
		super.getWindow().get(key).add(data);
		
		//remove first element if window is full
		if (super.getWindow().get(key).size() > super.getWindowSize()){
			
			super.getWindow().get(key).remove(0);
		}
	}

	/**
	 * returns the element for a given attribute and index in the window.
	 * @param key
	 * @param index
	 * @return
	 */
	public Serializable getElementAt(String key, Integer index){
		
		if (!this.containsKey(key) || index > this.getWindow(key).size() || index < 0)
			return null;
		
		return this.getWindow(key).get(index);
	}
	
	public boolean resetElementAt(String key, Integer index, Serializable value){
		
		if (this.getWindow(key).size() > index && index >= 0){
			
			this.getWindow(key).add(index, value);
			this.getWindow(key).remove(index+1);
			return true;
		}
		return false;
	}
}
