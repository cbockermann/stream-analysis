package stream.series.util;

import java.io.Serializable;
import java.util.Vector;

/**
 * This class provides an outline for distance functions on real valued spaces. Coordinates
 * must be provided as {@code Vector}s containing {@code Double} values for each dimension.
 * 
 * @author Markus Kokott ( markus.kokott(at)udo.edu )
 * 27.02.2012
 */
public abstract class Distance implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7977518993888146888L;

	public Double getDistance (Vector<Double> coordinates_a, Vector<Double> coordinates_b){
		
		if (coordinates_a.size() != coordinates_b.size() || coordinates_a.isEmpty()){
			
			return Double.NaN;
		}
		
		return this.computeDistance(coordinates_a, coordinates_b);
	}
	
	/**
	 * Override this method for computing a distance. 
	 * @param coordinates_a
	 * @param coordinates_b
	 * @return
	 */
	protected abstract Double computeDistance(Vector<Double> coordinates_a, Vector<Double> coordinates_b);
}
