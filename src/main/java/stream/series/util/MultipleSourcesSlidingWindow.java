package stream.series.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Vector;

import stream.AbstractProcessor;
import stream.data.Data;

/**
 * Similar to {@link traffix.windows.sliding#SimpleSlidingWindow}, this class
 * maintains a sliding window of size {@code windowSize} for each observed
 * attribute. In contrast to that sliding window, this one will differentiate
 * between different sources and creates a sliding window for each data source.
 * Data sources must have a unique id, specified by the given value for
 * {@sensorId}.
 * 
 * @author Markus Kokott ( markus.kokott(at)udo.edu ) 09.03.2012
 */
public class MultipleSourcesSlidingWindow extends AbstractProcessor {

	/* -----------mandatory parameters-------------- */
	private Integer windowSize;
	private String[] keys;
	/* --------------------------------------------- */

	/* -----------optional parameters--------------- */
	private String sensorId = "";
	private String prefix = "";
	/* --------------------------------------------- */

	private HashMap<String, SingleSourceData> windows;

	public MultipleSourcesSlidingWindow() {

		this.windows = new HashMap<String, SingleSourceData>();
	}

	public String[] getKeys() {
		return keys;
	}

	public void setKeys(String[] keys) {
		this.keys = keys;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSensorId() {
		return sensorId;
	}

	public void setSensorId(String sensorId) {
		this.sensorId = sensorId;
	}

	public Integer getWindowSize() {
		return windowSize;
	}

	public void setWindowSize(Integer windowSize) {
		this.windowSize = windowSize;
	}

	@Override
	public Data process(Data data) {

		if (this.sensorId != "" && !data.containsKey(this.sensorId))
			return data;

		String id = "none";
		if (this.sensorId != "" && data.containsKey(this.sensorId))
			id = (String) data.get(this.sensorId);

		if (!this.windows.containsKey(id))
			this.seenNewSensor(id);

		for (String key : this.getKeys()) {

			if (data.containsKey(key)) {

				if (!this.windows.get(id).containsDataForAttribut(key)) {

					this.seenNewKey(id, key);
				}
				this.update(id, key, data.get(key));

				if (this.isUpdateTime(id, key)) {

					@SuppressWarnings("unchecked")
					Vector<Serializable> vector = (Vector<Serializable>) this.windows
							.get(id).getDataForAttribute(key);
					data.put(this.getPrefix() + key, vector);
				}
			}
		}

		return data;
	}

	private void seenNewSensor(String id) {

		this.windows.put(id, new SingleSourceData(id));
	}

	private void seenNewKey(String id, String key) {

		this.windows.get(id).putDataForAttribute(key,
				new Vector<Serializable>());
	}

	private void update(String sensorId, String attribute, Serializable value) {

		@SuppressWarnings("unchecked")
		Vector<Serializable> temp = (Vector<Serializable>) this.windows.get(
				sensorId).getDataForAttribute(attribute);

		if (temp.size() == this.windowSize) {

			temp.remove(0);
		}
		temp.add(value);
		this.windows.get(sensorId).putDataForAttribute(attribute, temp);
	}

	@SuppressWarnings("unchecked")
	private boolean isUpdateTime(String id, String attribute) {

		if (((Vector<Serializable>) this.windows.get(id).getDataForAttribute(
				attribute)).size() == this.windowSize) {

			return true;
		}
		return false;
	}

	@Override
	public void finish() throws Exception {
	}
}
