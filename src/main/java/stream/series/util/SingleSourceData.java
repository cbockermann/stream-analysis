package stream.series.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

/**
 * This is a wrapper class for separating data of a data stream that
 * has multiple sources. Create an instance for each unique source id
 * in the data stream.<br>
 * Each source in turn can produce multiple attributes. So data has to
 * be inserted using a unique identifier for the attributes name. Please
 * note, that for generic purposes the data gets stored as values of
 * type {@code Serializable}. So there is no update function.  
 *  
 * @author Markus Kokott ( markus.kokott(at)udo.edu )
 * 09.03.2012
 */
public class SingleSourceData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5014018640054490039L;
	private String sourceId;
	private HashMap<String,Serializable> data;
	
	public SingleSourceData(String sourceId){
		
		this.sourceId = sourceId;
		this.data = new HashMap<String,Serializable>();
	}
	
	public String getSourceId(){
		
		return this.sourceId;
	}
	
	public Serializable getDataForAttribute(String attributeName){
		
		return this.data.get(attributeName);
	}
	
	/**
	 * the {@code Serializable} object for {@code attributeName} will
	 * be inserted. If a value for this attribute already exists, it
	 * gets replaced. So if you rather want to update the value of
	 * the attributes object, you have to call {@code 
	 * getDataForAttribute(attributeName)}, perform the update operation
	 * and call this method afterwards with the resulting value.
	 * 
	 * @param attributeName
	 * @param data
	 */
	public void putDataForAttribute(String attributeName, Serializable data){
		
		this.data.put(attributeName, data);
	}
	
	/**
	 * Returns true, if this instance of {@code SingleSourceData} contains
	 * a value for given {@code String} attributeName.
	 * 
	 * @param attributeName
	 * @return
	 */
	public boolean containsDataForAttribut(String attributeName){
		
		return this.data.containsKey(attributeName);
	}
	
	/**
	 * Returns a {@link java.util#Set} of {@code String}s. Set contains
	 * all stored keys.
	 * 
	 * @return
	 */
	public Set<String> keySet(){
		
		return this.data.keySet();
	}
	
	public boolean isEmpty(){
		
		return this.data.isEmpty();
	}
}
