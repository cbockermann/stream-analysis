package stream.series.util;

import java.io.Serializable;
import java.util.Vector;

public class SimpleMotifData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2682336235900579649L;
	private Vector<Double> values;
	private Integer matchCount;
	
	public SimpleMotifData(Vector<Double> values){
		
		this.values = values;
		this.matchCount = 0;
	}
	
	public void incrementCount(){
		
		this.matchCount++;
	}
	
	public Vector<Double> getValues() {
		return values;
	}

	public Integer getMatchCount() {
		return matchCount;
	}
	
	public String getLabel(){
		
		 String label = "[";
		 for (int i=0; i<values.size(); i++){
			 
			 label += this.values.get(i);
			 if (i == this.values.size() - 1){
				 
				 label += "]";
			 }
			 else{
				 
				 label += ",";
			 }
		 }
		 return label;
	}
	
	public String toString(){
		
		return this.getLabel();
	}
}
