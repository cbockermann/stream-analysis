package stream.series.util;

import java.util.Vector;

/**
 * simple implementation of euclidean distance for elements in real valued
 * spaces.
 * 
 * @author Markus Kokott ( markus.kokott(at)udo.edu ) 27.02.2012
 */
public class EuclideanDistance extends Distance {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2204313980397092277L;
	private Integer decimals = -1;

	public EuclideanDistance() {

	}

	public EuclideanDistance(Integer decimals) {

		if (decimals > 0) {
			this.decimals = decimals;
		}
	}

	@Override
	protected Double computeDistance(Vector<Double> coordinates_a,
			Vector<Double> coordinates_b) {

		Double sum = 0d;

		for (int i = 0; i < coordinates_a.size(); i++) {

			sum += Math.pow((coordinates_a.get(i) - coordinates_b.get(i)), 2);
		}

		if (this.decimals > 0) {

			return NumericValueTransformator.roundDouble(Math.sqrt(sum),
					this.decimals);
		}
		return Math.sqrt(sum);
	}

}
