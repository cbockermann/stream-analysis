package stream.series.util;

import java.io.Serializable;
import java.util.Vector;

public class NumericValueCaster {

	public static boolean isCastableToDouble(Serializable rawValue) {

		try {
			new Double(rawValue.toString());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static Double castToDouble(Serializable rawValue)
			throws ClassCastException {

		if (NumericValueCaster.isCastableToDouble(rawValue)) {

			return new Double(rawValue.toString());
		} else {

			return Double.NaN;
		}
	}

	public static boolean isCastableToInteger(Serializable rawValue) {

		try {
			new Integer(rawValue.toString());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static Integer castToInteger(Serializable rawValue)
			throws ClassCastException {

		if (NumericValueCaster.isCastableToInteger(rawValue)) {

			return new Integer(rawValue.toString());
		} else {

			return null;
		}
	}

	public static Vector<Double> castToDoubleVector(Serializable rawValue)
			throws ClassCastException {

		@SuppressWarnings("unchecked")
		Vector<Serializable> vector = (Vector<Serializable>) rawValue;
		Vector<Double> doubleVector = new Vector<Double>();

		for (Serializable element : vector) {

			if (NumericValueCaster.isCastableToDouble(element)) {
				doubleVector.add(NumericValueCaster.castToDouble(element));
			} else
				doubleVector.add(Double.NaN);
		}
		return doubleVector;
	}

	public static Vector<Integer> castToIntegerVector(Serializable rawValue)
			throws ClassCastException {

		@SuppressWarnings("unchecked")
		Vector<Serializable> vector = (Vector<Serializable>) rawValue;
		Vector<Integer> integerVector = new Vector<Integer>();

		for (Serializable element : vector) {

			if (NumericValueCaster.isCastableToInteger(element))
				integerVector.add(NumericValueCaster.castToInteger(element));
			else
				integerVector.add(Integer.MIN_VALUE);
		}
		return integerVector;
	}
}
