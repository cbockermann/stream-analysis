package stream.series.util;

/**
 * This class contains some static function for transforming a numeric value.
 * @author Markus Kokott ( markus.kokott(at)udo.edu )
 * 10.02.2012
 */
public class NumericValueTransformator {

	/**
	 * A probabilistic value is standardized iff both conditions are true:
	 * <ul>
	 * <li> its mean is zero
	 * <li> its standard deviation is one
	 * </ul>
	 * You can standardize a value <i>x</i> by computing its standard score.
	 * @param value - a {@code Double} value to be standardized.
	 * @param mean - a {@code Double} value representing {@code value}'s mean.
	 * @param standardDeviation - a {@code Double} value representing {@code value}'s standard deviation.
	 * @return standardized {@code Double} value for {@code value}.
	 */
	public static Double getStandardScore(Double value, Double mean, Double standardDeviation){
		
		return ((value - mean) / standardDeviation);
	}
	
	/**
	 * Rounds a given {@code Double} value {@code value} to a specified number
	 * of decimal places {@code decimals}.
	 * @param value
	 * @param decimals
	 * @return
	 */
	public static Double roundDouble(Double value, Integer decimals){
		
		Integer scaledValue = (int)(value * Math.pow(10, decimals));
		return scaledValue / Math.pow(10, decimals);
	}
}
