package stream.series.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Vector;

/**
 * This sliding window removes an element when it becomes too
 * old rather than using a constant window size as threshold.
 * Because of that, every time an element gets added, a time
 * stamp must be provided.<br>
 * For performance purposes, old elements get deleted when the
 * insert method is called. So, if there aren't new objects for
 * a long time, the received window might be out dated.
 * 
 * @author Markus Kokott ( markus.kokott(at)udo.edu )
 * 10.03.2012
 */
public class TemporalSlidingWindow implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 768326958199960458L;
	private HashMap<String, Vector<Long>> timeStamps;
	private HashMap<String, Vector<Serializable>> window;
	private Integer historySize=1;
	
	public TemporalSlidingWindow(Integer historySize){
		
		this.setHistorySize(historySize);
		this.timeStamps = new HashMap<String, Vector<Long>>();
		this.window = new HashMap<String,Vector<Serializable>>();
	}

	public Integer getHistorySize() {
		return historySize;
	}

	public void setHistorySize(Integer historySize) {
		
		if (historySize > 0){
			this.historySize = historySize;
		}
	}
	
	public void insertElement(String key, Long timeStamp, Serializable element){
		
		if (!this.window.containsKey(key)){
			
			this.seenNewKey(key);
		}
		this.timeStamps.get(key).add(timeStamp);
		this.window.get(key).add(element);
		
		this.update(key);
	}
	
	@SuppressWarnings("unchecked")
	public Vector<Serializable> getWindow(String key){
		
		return (Vector<Serializable>) this.window.get(key).clone();
	}
	
	private void seenNewKey(String key){
		
		this.window.put(key, new Vector<Serializable>());
		this.timeStamps.put(key, new Vector<Long>());
	}
	
	private void update(String key){
		
		Long now = this.timeStamps.get(key).get(this.timeStamps.get(key).size() - 1);
		while (this.timeStamps.get(key).get(0) < now - (this.historySize - 1)){
			
			this.timeStamps.get(key).remove(0);
			this.window.get(key).remove(0);
		}
	}
}
