package stream.series.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Vector;

public abstract class Window implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8755304003109987034L;

	private Integer windowSize;
	private HashMap<String, Vector<Serializable>> window;
	private Vector<String> keys;

	public Window(Integer windowSize) {

		this.setWindowSize(windowSize);
		this.keys = new Vector<String>();
		this.setWindow(new HashMap<String, Vector<Serializable>>());
	}

	public Integer getWindowSize() {
		return windowSize;
	}

	public void setWindowSize(Integer windowSize) {
		this.windowSize = windowSize;
	}

	public Vector<String> getKeys() {
		return keys;
	}

	public String[] getKeysAsArray() {
		String[] keys = new String[this.keys.size()];

		for (int i = 0; i < keys.length; i++) {
			keys[i] = this.keys.get(i);
		}

		return keys;
	}

	public void setKeys(Vector<String> keys) {
		this.keys = keys;
	}

	public void setKeys(String[] keys) {

		this.keys = new Vector<String>();
		for (String key : keys) {

			this.keys.add(key);
		}
	}

	protected HashMap<String, Vector<Serializable>> getWindow() {
		return window;
	}

	protected void setWindow(HashMap<String, Vector<Serializable>> window) {
		this.window = window;
	}

	public abstract Vector<Serializable> getWindow(String key);

	public abstract void updateWindow(String key, Serializable data);

	public boolean containsKey(String key) {

		return this.keys.contains(key);
	}

	public boolean isWindowFull(String key) {

		if (this.containsKey(key)) {

			if (this.getWindow(key).size() == this.getWindowSize()) {

				return true;
			}
		}
		return false;
	}
}
