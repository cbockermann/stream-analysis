package stream.series;

import java.io.Serializable;
import java.util.HashMap;

import stream.AbstractProcessor;
import stream.data.Data;
import stream.series.util.NumericValueCaster;
import stream.series.util.NumericValueTransformator;

/**
 * This simple class can be used to standardize some attributes in a data
 * stream. The standardized value can either replace the original value or
 * become an additional attribute of incoming {@code Data} objects. Use
 * {@code boolean} variable {@code overwrite} to switch between adding a new
 * attribute (standard behavior, value is {@code false}) or replace the original
 * value (set {@code true}).<br>
 * Please note, that this node doesn't maintain mean and standard deviation. It
 * expects to find those values in incoming {@code Data} objects. Use
 * {@link StreamStatistics} to get this values.
 * 
 * @author Markus Kokott ( markus.kokott(at)udo.edu ) 10.02.2012
 */
public class NumericValueNormalizer extends AbstractProcessor {

	private String meanPrefix;
	private String standardDeviationPrefix;
	private String[] keys;

	private String prefix = "";

	private HashMap<String, Double> stds;
	private HashMap<String, Double> means;

	public NumericValueNormalizer() {

		this.meanPrefix = "mean_";
		this.standardDeviationPrefix = "stDev_";
		this.stds = new HashMap<String, Double>();
		this.means = new HashMap<String, Double>();
	}

	public String getMeanPrefix() {
		return meanPrefix;
	}

	public void setMeanPrefix(String meanPrefix) {
		this.meanPrefix = meanPrefix;
	}

	public String getStandardDeviationPrefix() {
		return standardDeviationPrefix;
	}

	public void setStandardDeviationPrefix(String standardDeviationPrefix) {
		this.standardDeviationPrefix = standardDeviationPrefix;
	}

	public String[] getKeys() {
		return keys;
	}

	public void setKeys(String[] keys) {
		this.keys = keys;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@Override
	public Data process(Data data) {

		for (String key : this.getKeys()) {

			// updating mean and variance for 'key' if present
			if (data.containsKey(this.meanPrefix + key))
				this.updateMean(key, data.get(this.meanPrefix + key));
			if (data.containsKey(this.standardDeviationPrefix + key))
				this.updateStd(key,
						data.get(this.standardDeviationPrefix + key));

			if (data.containsKey(key) && this.means.containsKey(key)
					&& this.stds.containsKey(key)) {

				if (NumericValueCaster.isCastableToDouble(data.get(key))) {

					Double standardScore = NumericValueTransformator
							.getStandardScore(NumericValueCaster
									.castToDouble(data.get(key)), this.means
									.get(key), this.stds.get(key));

					data.put(this.getPrefix() + key, standardScore);
				}
			}
		}

		return data;
	}

	private void updateMean(String key, Serializable mean) {

		if (NumericValueCaster.isCastableToDouble(mean))
			this.means.put(key, NumericValueCaster.castToDouble(mean));
	}

	private void updateStd(String key, Serializable std) {

		if (NumericValueCaster.isCastableToDouble(std))
			this.stds.put(key, NumericValueCaster.castToDouble(std));
	}

	@Override
	public void finish() throws Exception {
	}
}
