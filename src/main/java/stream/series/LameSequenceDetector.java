package stream.series;

import java.io.Serializable;
import java.util.Vector;

import stream.AbstractProcessor;
import stream.data.Data;
import stream.series.util.NumericValueCaster;

public class LameSequenceDetector extends AbstractProcessor {

	public static String LAME_SEQUENCE_DETECTOR_LABEL = "@lameSequence";

	private String[] keys;
	private Double minDifference;

	private boolean removeLameSequences = false;
	private String lameSequenceLabel = LAME_SEQUENCE_DETECTOR_LABEL;

	public String[] getKeys() {
		return keys;
	}

	public void setKeys(String[] keys) {
		this.keys = keys;
	}

	public String getLameSequenceLabel() {
		return lameSequenceLabel;
	}

	public void setLameSequenceLabel(String lameSequenceLabel) {
		this.lameSequenceLabel = lameSequenceLabel;
	}

	public boolean isRemoveLameSequences() {
		return removeLameSequences;
	}

	public void setRemoveLameSequences(boolean removeLameSequences) {
		this.removeLameSequences = removeLameSequences;
	}

	public Double getMinDifference() {
		return minDifference;
	}

	public void setMinDifference(Double minDifference) {
		this.minDifference = minDifference;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Data process(Data data) {

		for (String key : this.getKeys()) {

			if (data.containsKey(key)) {

				Vector<Serializable> vector = (Vector<Serializable>) data
						.get(key);
				if (this.removeLameSequences) {

					data.remove(key);
				}
				if (this.instanceOfVectorOfDoubles(vector)) {

					if (this.getMaxDifferenceOfVector(vector) >= this.minDifference) {

						// TODO have to change this in case of more than one
						// key. perhaps a map or prefix?
						data.put(this.lameSequenceLabel, new Boolean(true));
						data.put(key, vector);
					}
				}
			}
		}
		return data;
	}

	private Double getMaxDifferenceOfVector(Vector<Serializable> vector) {

		Double minValue = Double.MAX_VALUE;
		Double maxValue = -Double.MAX_VALUE;

		for (Serializable element : vector) {

			Double value = NumericValueCaster.castToDouble(element);
			if (value > maxValue) {
				maxValue = value;
			}
			if (value < minValue) {
				minValue = value;
			}
		}
		return (maxValue - minValue);
	}

	private boolean instanceOfVectorOfDoubles(Vector<Serializable> vector) {

		for (Serializable element : vector) {

			if (!NumericValueCaster.isCastableToDouble(element)) {

				return false;
			}
		}
		return true;
	}

	@Override
	public void finish() throws Exception {
	}

}
