package stream.series;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Vector;

import stream.AbstractProcessor;
import stream.data.Data;
import stream.series.util.NumericValueCaster;
import stream.series.util.TemporalSlidingWindow;

/**
 * This class computes the average over a sliding window to deal with noisy data
 * streams.
 * 
 * @author Markus Kokott ( markus.kokott(at)udo.edu ) 10.03.2012
 */
public class NoiseReducer extends AbstractProcessor {

	private HashMap<String, TemporalSlidingWindow> windows;

	/* ---------mandatory parameters----------- */
	private String timeKey;
	private String[] keys;
	/* ---------------------------------------- */

	/* --------optional parameters------------ */
	private String sensorId = "";
	private Integer timeHorizon = 5;
	private String prefix = "";

	/* ---------------------------------------- */

	public NoiseReducer() {

		this.windows = new HashMap<String, TemporalSlidingWindow>();
	}

	public String[] getKeys() {
		return keys;
	}

	public void setKeys(String[] keys) {
		this.keys = keys;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public Integer getTimeHorizon() {
		return timeHorizon;
	}

	public void setTimeHorizon(Integer timeHorizon) {

		if (timeHorizon > 0) {
			this.timeHorizon = timeHorizon;
		}
	}

	public String getSensorId() {
		return sensorId;
	}

	public void setSensorId(String sensorId) {
		this.sensorId = sensorId;
	}

	public String getTimeKey() {
		return timeKey;
	}

	public void setTimeKey(String timeKey) {
		this.timeKey = timeKey;
	}

	@Override
	public Data process(Data data) {

		if (this.sensorId != "" && !data.containsKey(this.sensorId))
			return data;

		String id = "none";
		if (data.containsKey(this.sensorId) && this.sensorId != "")
			sensorId = (String) data.get(this.sensorId);

		if (data.containsKey(this.sensorId) && data.containsKey(this.timeKey)) {

			Long timeStamp = (Long) data.get(this.timeKey);
			for (String key : this.getKeys()) {

				if (data.containsKey(key)) {

					this.update(id, key, data.get(key), timeStamp);
					data.put(this.getPrefix() + key, this.computeValue(id, key));
				}
			}
		}

		return data;
	}

	/**
	 * Computes the average of current window.
	 * 
	 * @param id
	 * @param key
	 * @return
	 */
	private Double computeValue(String id, String key) {

		Double average = 0d;
		Vector<Serializable> window = this.windows.get(id).getWindow(key);
		Integer badValues = 0;
		for (Serializable element : window) {

			if (NumericValueCaster.isCastableToDouble(element)) {

				average += NumericValueCaster.castToDouble(element);
			} else {

				badValues++;
			}
		}
		if (window.size() - badValues > 0) {
			return (average / (window.size() - badValues));
		}
		return Double.NaN;
	}

	/**
	 * inserts a new element in the sliding window.
	 * 
	 * @param id
	 * @param key
	 * @param data
	 * @param timeStamp
	 */
	private void update(String id, String key, Serializable data, Long timeStamp) {

		if (!this.windows.containsKey(id)) {

			this.seenNewSensor(id);
		}
		this.windows.get(id).insertElement(key, timeStamp, data);
	}

	private void seenNewSensor(String id) {

		this.windows.put(id, new TemporalSlidingWindow(this.timeHorizon));
	}

	@Override
	public void finish() throws Exception {
	}
}
