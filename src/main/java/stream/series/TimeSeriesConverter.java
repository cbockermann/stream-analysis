package stream.series;

import java.io.Serializable;
import java.util.HashMap;

import stream.AbstractProcessor;
import stream.data.Data;

public class TimeSeriesConverter extends AbstractProcessor {

	private HashMap<String, Long> indices;
	private HashMap<String, Long> startValues;

	private Long minStepSize;
	private String timeKey;

	private String sensorId = "";

	public TimeSeriesConverter() {

		this.indices = new HashMap<String, Long>();
		this.startValues = new HashMap<String, Long>();
	}

	public Long getMinStepSize() {
		return minStepSize;
	}

	/**
	 * The {@code minStepSize} is a {@code Long} value usually providing an time
	 * interval in milliseconds. e.g. if you set its value to 60000, the
	 * resulting intervals are 1 minute. If a {@code Data} object contains an
	 * element that is just 59 seconds younger than the previous one, it will be
	 * discarded.
	 * 
	 * @param minStepSize
	 */
	public void setMinStepSize(Long minStepSize) {

		if (minStepSize > 0) {
			this.minStepSize = minStepSize;
		}
	}

	public String getSensorId() {
		return sensorId;
	}

	public void setSensorId(String sensorId) {
		this.sensorId = sensorId;
	}

	public String getTimeKey() {
		return timeKey;
	}

	public void setTimeKey(String timeKey) {
		this.timeKey = timeKey;
	}

	@Override
	public Data process(Data data) {

		/*
		 * if either there is no timeKey or a sensorId is specified and not
		 * present, nothing is to be done
		 */
		if (!data.containsKey(this.timeKey)
				|| (this.sensorId != "" && !data.containsKey(this.sensorId)))
			return data;

		String sensorId = "none";
		if (this.sensorId != "" && data.containsKey(this.sensorId))
			sensorId = (String) data.get(this.sensorId);

		if (!this.indices.containsKey(sensorId)) {

			this.seenNewKey(sensorId,
					this.castTimeStamp(data.get(this.timeKey)));
			data.put(this.timeKey, 0l);
			return data;
		}

		Long id = this.getTimeseriesId(sensorId,
				this.castTimeStamp(data.get(this.timeKey)));
		if (id > 0) {
			data.put(this.timeKey, id);
		}
		return data;
	}

	/**
	 * this method checks if the provided {@code date} is younger than the last
	 * known one for {@code key}. Also, it checks whether the
	 * {@code minStepSize} holds for the given date.
	 * 
	 * @param key
	 * @param date
	 * @return
	 */
	private Long getTimeseriesId(String key, Long date) {

		if (date >= this.indices.get(key) + this.minStepSize) {

			this.indices.put(key, date);

			Long index = ((this.indices.get(key) - this.startValues.get(key)) / this.minStepSize);

			return index;
		}

		return -1904l;
	}

	/**
	 * just adds a new index to {@code indices} if a new {@code sensorId}
	 * appears.
	 * 
	 * @param key
	 */
	private void seenNewKey(String key, Long date) {

		this.indices.put(key, date);
		this.startValues.put(key, date);
	}

	@Override
	public void finish() throws Exception {
	}

	private Long castTimeStamp(Serializable ts) {

		try {
			return (Long) ts;
		} catch (ClassCastException cce) {
		}

		try {
			return Long.parseLong((String) ts);
		} catch (Exception e) {

		}

		return null;
	}

}
