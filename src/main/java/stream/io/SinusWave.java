/**
 * 
 */
package stream.io;

import stream.data.Data;

/**
 * @author chris
 * 
 */
public class SinusWave extends AbstractDataStream {

	Double index = 0.0d;
	Double amplitude = 1.0;
	Double frequency = 1.0;

	/**
	 * @see stream.io.DataStream#close()
	 */
	@Override
	public void close() throws Exception {

	}

	/**
	 * @see stream.io.AbstractDataStream#readHeader()
	 */
	@Override
	public void readHeader() throws Exception {
	}

	/**
	 * @see stream.io.AbstractDataStream#readItem(stream.data.Data)
	 */
	@Override
	public Data readItem(Data instance) throws Exception {
		Double value = amplitude * Math.sin(frequency * index);
		instance.put("t", index);
		instance.put("sin(t)", value);
		index += 0.01;
		return instance;
	}

	/**
	 * @return the amplitude
	 */
	public Double getAmplitude() {
		return amplitude;
	}

	/**
	 * @param amplitude
	 *            the amplitude to set
	 */
	public void setAmplitude(Double amplitude) {
		this.amplitude = amplitude;
	}

	/**
	 * @return the frequency
	 */
	public Double getFrequency() {
		return frequency;
	}

	/**
	 * @param frequency
	 *            the frequency to set
	 */
	public void setFrequency(Double frequency) {
		this.frequency = frequency;
	}
}
