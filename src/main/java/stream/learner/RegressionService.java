package stream.learner;

import stream.data.Data;
import stream.service.Service;

public interface RegressionService extends Service {

	public Double predict( Data item );
}
