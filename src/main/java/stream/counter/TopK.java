/**
 * 
 */
package stream.counter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.ProcessContext;
import stream.data.Data;

/**
 * @author chris
 * 
 */
public class TopK extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(TopK.class);
	String key;

	Integer k = 10;
	SimpleTopKCounting counter;

	String file;

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);

		if (key == null)
			throw new Exception("Missing 'key' parameter!");

		counter = new SimpleTopKCounting(k);
	}

	/**
	 * @see stream.Processor#process(stream.data.Data)
	 */
	@Override
	public Data process(Data input) {

		Serializable value = input.get(key);
		if (value != null) {
			counter.count(value.toString());

			if (log.isDebugEnabled()) {
				log.debug("Counting {}", value);
				for (String elem : counter.keySet()) {
					log.debug(" {} = {}", elem, counter.getCount(elem));
				}
			}
		}
		return input;
	}

	/**
	 * @see stream.AbstractProcessor#finish()
	 */
	@Override
	public void finish() throws Exception {
		super.finish();

		if (file != null) {
			File f = new File(file);
			f.getParentFile().mkdirs();
			log.info("Writing current top-{} elements to {}", k, f);
			PrintStream out = new PrintStream(new FileOutputStream(f));
			for (String elem : counter.keySet()) {
				out.println(elem + "\t" + counter.getCount(elem));
			}
			out.close();
		}
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the k
	 */
	public Integer getK() {
		return k;
	}

	/**
	 * @param k
	 *            the k to set
	 */
	public void setK(Integer k) {
		this.k = k;
	}

	/**
	 * @return the file
	 */
	public String getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(String file) {
		this.file = file;
	}
}