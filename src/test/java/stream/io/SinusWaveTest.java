/**
 * 
 */
package stream.io;

import java.net.URL;

/**
 * @author chris
 * 
 */
public class SinusWaveTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		URL url = SinusWaveTest.class.getResource("/sinus-wave.xml");
		stream.run.main(url);
	}
}
