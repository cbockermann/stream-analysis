/**
 * 
 */
package stream.io;

import java.net.URL;

/**
 * @author chris
 * 
 */
public class StockMotifs {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		URL url = StockMotifs.class.getResource("/stock-motifs.xml");
		stream.run.main(url);
	}
}
